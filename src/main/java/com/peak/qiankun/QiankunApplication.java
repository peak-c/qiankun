package com.peak.qiankun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QiankunApplication {

	public static void main(String[] args) {
		SpringApplication.run(QiankunApplication.class, args);
	}

}
