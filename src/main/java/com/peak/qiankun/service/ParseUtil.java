package com.peak.qiankun.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.*;

public class ParseUtil {

    private static Map<String, String> map;

    static {
        map = new HashMap<String, String>();
        map.put("上", "_");

        map.put("乾", "1");
        map.put("兑", "2");
        map.put("离", "3");
        map.put("震", "4");
        map.put("巽", "5");
        map.put("坎", "6");
        map.put("艮", "7");
        map.put("坤", "8");

    }

    public static void parse(String file) throws Exception {

        File input = new File(file);
        Element body = Jsoup.parse(input, "UTF-8").body();

        String title = body.getElementsByClass("article-title").get(0).child(0).text();
        title = simpleTitle(title);

        String benGua = getZhugua(title);
        String basePath = "/Users/peak/Documents/git/qiankun/doc/";
        String writeFile = "";

        Element content = body.getElementsByClass("article-content").get(0);

        StringBuilder sb = null;
        int index = 0;
        for (Element child : content.children()) {

            Elements h2s = child.getElementsByTag("h2");
            if (CollUtil.isNotEmpty(h2s)) {
                saveToFile(sb, writeFile);

                sb = new StringBuilder();

                if (index == 0) {
                    //本卦
                    writeFile = basePath + benGua + ".md";
                    sb.append("# " + title + "\n");
                    sb.append("## 本卦" + "\n");
                } else {
                    //爻辞
                    writeFile = basePath + benGua + "_" + index + ".md";
                    sb.append("## 爻辞" + "\n");
                }
                index++;
//                System.out.println(writeFile);
            }
            Elements h15s = child.getElementsByClass("h15");
            if (CollUtil.isNotEmpty(h15s)) {
                try {
                    String part = h15s.get(0).child(0).child(0).text();
                    sb.append("\n" + "**" + part + "**" + "\n");
                } catch (Exception e) {
                    String part = h15s.get(0).child(0).text();
                    sb.append("\n" + "**" + part + "**" + "\n");
                }
            }
            Elements gua_wens = child.getElementsByClass("gua_wen");
            if (CollUtil.isNotEmpty(gua_wens)) {
                Elements guas = gua_wens.get(0).children();
                for (Element gua : guas) {
                    try {
                        String guaText = gua.child(0).text();
                        sb.append("- " + guaText + "\n");
                    } catch (Exception e) {
                        System.out.println(gua);
                    }
                }
            }
            if (CollUtil.isEmpty(h2s) && CollUtil.isEmpty(h15s) && CollUtil.isEmpty(gua_wens)) {
                //特殊情况
                Elements divs = child.getElementsByTag("div");
                if (CollUtil.isNotEmpty(divs)) {
                    Elements ps = divs.get(0).getElementsByTag("p");
                    if (CollUtil.isNotEmpty(ps)) {

                        if (ps.size() > 1) {
                            String part = ps.get(0).getElementsByTag("span").get(0).text();
                            sb.append("\n" + "**" + part + "**" + "\n");

                            for (int i = 1; i < ps.size(); i++) {
                                try {
                                    String guaText = ps.get(i).getElementsByTag("span").get(0).text();
                                    sb.append("- " + guaText + "\n");
                                } catch (Exception e) {
                                    System.out.println(writeFile);
                                    System.out.println(ps.get(i));
                                }
                            }
                        } else {


                            if (index > 0) {

                                String part = ps.get(0).getElementsByTag("span").get(0).text();
                                if (part.contains("变卦")) {
                                    sb.append("\n" + "**" + part + "**" + "\n");
                                } else if (part.contains("变得周易")) {
                                    sb.append("- " + part + "\n");
                                }


                            }

                        }

                    }

                }


            }


        }

        saveToFile(sb, writeFile);

    }

    private static void saveToFile(StringBuilder sb, String writeFile) {
        if (null != sb) {
            //后处理
            String str = sb.toString().replaceAll("台湾", "湾湾");
            str = str.replaceAll("- 白话文解释", "\n**白话文解释**");

            //- 九二变卦
            str = str.replaceAll("- 初九变卦", "\n**初九变卦**");
            str = str.replaceAll("- 九二变卦", "\n**九二变卦**");
            str = str.replaceAll("- 九三变卦", "\n**九三变卦**");
            str = str.replaceAll("- 九四变卦", "\n**九四变卦**");
            str = str.replaceAll("- 九五变卦", "\n**九五变卦**");
            str = str.replaceAll("- 上九变卦", "\n**上九变卦**");

            str = str.replaceAll("- 初六变卦", "\n**初六变卦**");
            str = str.replaceAll("- 六二变卦", "\n**六二变卦**");
            str = str.replaceAll("- 六三变卦", "\n**六三变卦**");
            str = str.replaceAll("- 六四变卦", "\n**六四变卦**");
            str = str.replaceAll("- 六五变卦", "\n**六五变卦**");
            str = str.replaceAll("- 上六变卦", "\n**上六变卦**");


            FileUtil.writeUtf8String(str, writeFile);
        }
    }

    private static String getZhugua(String title) {
        //周易第3卦_屯卦(水雷屯)_坎上震下
        String temp = title.substring(title.length() - 4, title.length());
        String zhugua = "";
        for (char c : temp.toCharArray()) {
            zhugua = zhugua + map.getOrDefault(String.valueOf(c), "");

        }

        return zhugua;
    }

    private static String simpleTitle(String title) {
        //周易第3卦_屯卦(水雷屯)_坎上震下_上坎下震_周易六十四卦详解;
        title = title.substring(0, title.length() - 14);
        //周易第3卦_屯卦(水雷屯)_坎上震下
        return title;
    }

    public static void main(String[] args) throws Exception {

//        ParseUtil.parse("/Users/peak/Desktop/乾坤/周易第1卦_乾卦(乾为天)_乾上乾下_上乾下乾_周易六十四卦详解-易学界.html");

//        String basePath = "/Users/peak/Desktop/乾坤/";
//
//        List<String> files = FileUtil.listFileNames(basePath);
//        for (String file : files) {
//            String fileName = basePath + file;
////            System.out.println(file);
//
//            try {
//                ParseUtil.parse(fileName);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }


//        String path = "/Users/peak/Documents/git/qiankun/doc/";
//        List<String> files = FileUtil.listFileNames(path);
//        Set<String> set = new HashSet<>(files);
//
//        for (int i = 1; i <= 8; i++) {
//            for (int j = 1; j <= 8; j++) {
//                String bengua = i + "_" + j;
//                String benguaStr = bengua + ".md";
//                if (!set.contains(benguaStr)) {
//                    System.out.println(benguaStr);
//                }
//                for (int k = 1; k <= 6; k++) {
//                    String yaoci = bengua + "_" + k + ".md";
//                    if (!set.contains(yaoci)) {
//                        System.out.println(yaoci);
//                    }
//                }
//            }
//        }

//        String path = "/Users/peak/Documents/git/qiankun/doc/";
//        List<String> files = FileUtil.listFileNames(path);
//        int count = 0;
//        int count1 = 0;
//        for (String file : files) {
//            String temp = FileUtil.readUtf8String(path + file);
//            if (temp.length() < 50) {
//                count++;
//                System.out.println(file);
//            }
//
//            if (file.split("_").length ==3 && !temp.contains("变卦")) {
//                System.out.println("----->" + file);
//                count1++;
//            }
//        }
//        System.out.println(count);
//        System.out.println(count1);


        int count = 0;
        String base = "https://gitee.com/peak-c/qiankun/raw/master/doc/";
        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                String bengua = i + "_" + j;
                String benguaStr = bengua + ".md";

                String str = HttpUtil.get(base + benguaStr);
                if (str.contains("违规内容")) {
                    System.out.println(benguaStr);
                } else {
                    System.out.println(count++);
                }

                for (int k = 1; k <= 6; k++) {
                    String yaoci = bengua + "_" + k + ".md";
                    String res = HttpUtil.get(base + yaoci);
                    if (res.contains("违规内容")) {
                        System.out.println(yaoci);
                    } else {
                        System.out.println(count++);
                    }
                }
            }
        }


    }


}
